const express = require("express");
const app = express();

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/html/index.html");
});
app.get("/sobre", function (req, res) {
  res.sendFile(__dirname + "/html/sobre.html");
});
app.get("/blog", function (req, res) {
  res.sendFile("Bem-vido ao meu blog!");
});

app.get("/ola/:cargo/:nome/:cor", function (req, res) {
  res.send(
    "<h1> Ola " +
      req.params.nome +
      "</h1>" +
      "<h2> Seu cargo eh: " +
      req.params.cargo +
      "</h2>" +
      "<h3> Sua cor favorita eh: " +
      req.params.cor +
      "</h3>"
  );
});

app.listen(8081, function () {
  console.log("Servidor Rodando na url //localhost:8081");
});
