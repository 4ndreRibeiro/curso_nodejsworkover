var somaFunc = require("./soma");
var subFunc = require("./sub");
var multiFunc = require("./multi");
var divFunc = require("./div");

console.log("A soma é: " + somaFunc(1, 2));
console.log("A subtração é: " + subFunc(12, 4));
console.log("A multiplição é: " + multiFunc(5, 3));
console.log("A divisão é: " + divFunc(3, 2));